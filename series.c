#include<stdio.h>
int fact(int n)
{
    int i,prod=1;
    for(i=1;i<=n;i++)
    prod=prod*i;
    return prod;
}

int main()
{
   int n,i;
   double j,sum=1.0;
   printf("Enter N Value\n");
   scanf("%d",&n);
   for(i=2;i<=n;i++)
   {
	j=i;        
	j=j/fact(i);
	if((i%2)==0)
          sum=sum+j;
        else
          sum=sum-j;
   }
   printf("Sum Of N Terms Is %lf\n",sum);
   return 0;
}