#include<stdio.h>
int main()
{
   int arr[20],small,large,spos,lpos,i,n,temp;
   printf("Enter Number Of Elements You Want To Enter\n");
   scanf("%d",&n);
   printf("Enter %d Numbers\n",n);
   for(i=0;i<n;i++)
   scanf("%d",&arr[i]);
   small=arr[0];
   large=small;
   lpos=0;
   spos=0;
   for(i=1;i<n;i++)
   {
       if(arr[i]<small)
       {
            small=arr[i];
            spos=i;
       }
   }
   for(i=1;i<n;i++)
   {
       if(arr[i]>large)
       {
            large=arr[i];
            lpos=i;
       }
   } 
   temp=arr[lpos];
   arr[lpos]=arr[spos];
   arr[spos]=temp;
   printf("Largest Number:%d\nSmallest Number:%d\nModified Array:\n",large,small);
   for(i=0;i<n;i++)
   printf("%d\t",arr[i]);
   printf("\n");
   return 0;
}